# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-22 10:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('charityDonations', '0002_auto_20170221_1449'),
    ]

    operations = [
        migrations.AddField(
            model_name='donor',
            name='complement',
            field=models.CharField(blank=True, max_length=256),
        ),
        migrations.AddField(
            model_name='donor',
            name='number',
            field=models.CharField(blank=True, max_length=256),
        ),
        migrations.AddField(
            model_name='donor',
            name='postalCode',
            field=models.CharField(blank=True, max_length=256),
        ),
        migrations.AddField(
            model_name='donor',
            name='state',
            field=models.CharField(blank=True, max_length=256),
        ),
        migrations.AddField(
            model_name='entity',
            name='complement',
            field=models.CharField(blank=True, max_length=256),
        ),
        migrations.AddField(
            model_name='entity',
            name='number',
            field=models.CharField(blank=True, max_length=256),
        ),
        migrations.AddField(
            model_name='entity',
            name='postalCode',
            field=models.CharField(blank=True, max_length=256),
        ),
        migrations.AddField(
            model_name='entity',
            name='state',
            field=models.CharField(blank=True, max_length=256),
        ),
    ]
