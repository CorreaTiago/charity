from __future__ import unicode_literals

from django.db import models

class Donor (models.Model):
    __tablename__ = "donor"

    name = models.CharField(max_length=256)
    email = models.CharField(max_length=256)
    phone = models.CharField(max_length=256, blank=True)
    address = models.CharField(max_length=256, blank=True)
    number = models.CharField(max_length=256, blank=True)       
    complement = models.CharField(max_length=256, blank=True)       
    state = models.CharField(max_length=256, blank=True)       
    postalCode = models.CharField(max_length=256, blank=True)
    country = models.ForeignKey('Country', on_delete=models.CASCADE)            
    lgt = models.CharField(max_length=256, blank=True)       
    lat = models.CharField(max_length=256, blank=True)  
    items = models.ForeignKey('Donation', on_delete=models.CASCADE)

class Entity (models.Model):
    __tablename__ = "entity"

    name = models.CharField(max_length=256)
    email = models.CharField(max_length=256)
    phone = models.CharField(max_length=256, blank=True)
    address = models.CharField(max_length=256)       
    number = models.CharField(max_length=256, blank=True)       
    complement = models.CharField(max_length=256, blank=True)       
    state = models.CharField(max_length=256, blank=True)       
    postalCode = models.CharField(max_length=256, blank=True)  
    country = models.ForeignKey('Country', on_delete=models.CASCADE)     
    lgt = models.CharField(max_length=256)       
    lat = models.CharField(max_length=256)       
    photos = models.CharField(max_length=256, blank=True)              

class History (models.Model):
    __tablename__ = "history"
    
    description = models.CharField(max_length=256)
    entityId = models.ForeignKey('Entity', on_delete=models.CASCADE)
    items = models.ForeignKey('Donation', on_delete=models.CASCADE)

class Donation (models.Model):
    __tablename__ = "donation"
    
    description = models.CharField(max_length=256)    

class Country (models.Model):
    __tablename__ = "country"
    
    code = models.CharField(max_length=3)
    name = models.CharField(max_length=256)        