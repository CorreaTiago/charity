from django.http import HttpResponse,HttpResponseRedirect,Http404
from django.core import serializers
from charityDonations.models import Donor, Donation
import json

from django.views.decorators.csrf import csrf_exempt


def getDonor(request):
    d = Donor.objects.all()    
    donorData = serializers.serialize("json", d)
    return HttpResponse(donorData)

@csrf_exempt
def setDonor(request):
	#{
 	#	"name":"Tiago Correa",
 	#	"email":"tiagoc.mail@gmail.com",
 	#	"phone":"+552127210228",
 	#	"address":"Rua Passo da Patria",
 	#	"number": 83,
 	#	"complement":"205",
 	#	"state":"RJ",
 	#	"postalCode":"27511-010",
 	#	"country":"BRA",
 	#	"lgt":"641541654646464645",
 	#	"lat":"264541655416565", 
 	#   "items":1	
	#}	
    donorData = json.loads(request.body)
    donationItem = Donation.objects.get(id = donorData['items'])
    d = Donor(name = donorData['name'], email = donorData['email'], phone = donorData['phone'], address = donorData['address'], number = donorData['number'], complement = donorData['complement'], state = donorData['state'], postalCode = donorData['postalCode'], lgt = donorData['lgt'], lat = donorData['lat'], items = donationItem)        
    d.save()
    return HttpResponse(donorData)