from django.http import HttpResponse,HttpResponseRedirect,Http404
from django.core import serializers
from charityDonations.models import Entity
import json

from django.views.decorators.csrf import csrf_exempt


def getEntity(request):
    e = Entity.objects.all()    
    entityData = serializers.serialize("json", e)
    return HttpResponse(entityData)

@csrf_exempt
def setEntity(request):
	#{
 	#	"name":"Tiago Correa",
 	#	"email":"tiagoc.mail@gmail.com",
 	#	"phone":"+552127210228",
 	#	"address":"Rua Passo da Patria",
 	#	"number": 83,
 	#	"complement":"205",
 	#	"state":"RJ",
 	#	"postalCode":"27511-010",
 	#	"country":"BRA",
 	#	"lgt":"641541654646464645",
 	#	"lat":"264541655416565",
 	#	"photos":"me.jpg"
	#}

    entityData = json.loads(request.body)
    e = Entity(name = entityData['name'], email = entityData['email'], phone = entityData['phone'], address = entityData['address'], number = entityData['number'], complement = entityData['complement'], state = entityData['state'], postalCode = entityData['postalCode'], lgt = entityData['lgt'], lat = entityData['lat'], photos = entityData['photos'])    
    e.save()
    return HttpResponse(entityData)