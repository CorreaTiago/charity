from django.http import HttpResponse,HttpResponseRedirect,Http404
from django.core import serializers
from charityDonations.models import History
import json

from django.views.decorators.csrf import csrf_exempt

def getHistory(request):
    h = History.objects.all()    
    historyData = serializers.serialize("json", h)
    return HttpResponse(historyData)

@csrf_exempt
def setHistory(request):
	#{
 	#	"description":"Ajude nesse natal",
	#}
    historyData = json.loads(request.body)
    h = History(description = historyData['description'])        
    h.save()
    return HttpResponse(historyData)
	
    #entityId = models.ForeignKey('Entity', on_delete=models.CASCADE)
    #items = models.ForeignKey('Donation', on_delete=models.CASCADE)