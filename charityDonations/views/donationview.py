from django.http import HttpResponse,HttpResponseRedirect,Http404
from django.core import serializers
from charityDonations.models import Donation
import json

from django.views.decorators.csrf import csrf_exempt


def getDonation(request):
    d = Donation.objects.all()    
    donationData = serializers.serialize("json", d)
    return HttpResponse(donationData)

@csrf_exempt
def setDonation(request):
    donationData = json.loads(request.body)    
    d = Donation(description = donationData['description'])
    d.save()
    return HttpResponse(donationData)