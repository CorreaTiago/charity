from django.http import Http404 
from django.http import HttpResponse 
from django.template import RequestContext, loader 
import json
import sys, traceback

def index(request): 	
	template = loader.get_template('index.html') 
	context = RequestContext(request) 
	return HttpResponse(template.render(context)) 