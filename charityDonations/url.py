from django.conf.urls import url 
from . import views 

urlpatterns = [ url(r'^$', views.index, name='index'), 
				url(r'^api/getEntity/$', views.getEntity, name='getEntity'),
				url(r'^api/setEntity/$', views.setEntity, name='setEntity'),
				url(r'^api/getDonor/$', views.getDonor, name='getDonor'),
				url(r'^api/setDonor/$', views.setDonor, name='setDonor'),
				url(r'^api/getDonation/$', views.getDonation, name='getDonation'),
				url(r'^api/setDonation/$', views.setDonation, name='setDonation'),
				url(r'^api/getHistory/$', views.getHistory, name='getHistory'),
				url(r'^api/setHistory/$', views.setHistory, name='setHistory'),
			]				